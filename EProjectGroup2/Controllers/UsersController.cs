﻿using EProjectGroup2.DataContext;
using EProjectGroup2.Models;
using EProjectGroup2.Models.DataTransferObject.UserModelDto;
using Microsoft.AspNetCore.Mvc;

namespace EProjectGroup2.Controllers
{
    public class UsersController : Controller
    {
        private readonly ClinicDataContext _clinicalDataContext;
        public UsersController(ClinicDataContext clinicDataContext)
        {
            _clinicalDataContext = clinicDataContext;
        }
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(AddUserModelDto userModelDto)
        {
            var user = new UserModel()
            {
                UserName = userModelDto.UserName,
                Email = userModelDto.Email,
                Address = userModelDto.Address,
                Description = userModelDto.Description,
                DoB = userModelDto.DoB,
                FullName = userModelDto.FullName,
                IsAdmin = userModelDto.IsAdmin,
                IsDelete = userModelDto.IsDelete,
                Password = userModelDto.Password,
                Phone = userModelDto.Phone,
                Sex = userModelDto.Sex,
                Status = userModelDto.Status,
            };
            await _clinicalDataContext.Users.AddAsync(user);
            await _clinicalDataContext.SaveChangesAsync();
            return RedirectToAction("Add");
        }
    }
}
