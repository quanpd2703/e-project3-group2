﻿using EProjectGroup2.Models;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.DataContext
{
    public class ClinicDataContext : DbContext
    {
        public ClinicDataContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<UserModel> Users { get; set; }
        public DbSet<PharmacyModel> Pharmacies { get; set; }
        public DbSet<MedicalEquipmentModel> MedicalEquipments { get; set; }
        public DbSet<EducationModel> Educations { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<OrdersModel> Orders { get; set; }
        public DbSet<ReviewsModel> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserLogin>().ToTable("UserLogins");
            builder.Entity<UserLogin>().HasKey(t => t.UserID);

            builder.Entity<UserModel>().ToTable("Users");
            builder.Entity<UserModel>().HasKey(t=>t.UserId);

            builder.Entity<PharmacyModel>().ToTable("Pharmacies");
            builder.Entity<PharmacyModel>().HasKey(t => t.PharmacyID);

            builder.Entity<MedicalEquipmentModel>().ToTable("MedicalEquipments");
            builder.Entity<MedicalEquipmentModel>().HasKey(t => t.MedicalEquipmentID);

            builder.Entity<EducationModel>().ToTable("Educations");
            builder.Entity<EducationModel>().HasKey(t => t.EducationId);
            builder.Entity<EducationModel>()
                .HasOne(a=>a.Users)
                .WithMany(x=>x.Educations)
                .HasForeignKey(y=>y.UserID)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Entity<OrdersModel>().ToTable("Orders");
            builder.Entity<OrdersModel>().HasKey(t => t.OrderID);
            builder.Entity<OrdersModel>(entity =>
            {
                entity.HasOne(a => a.Users)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.UserID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.Pharmacies)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.PharmacyID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.MedicalEquipments)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.MedicalEquipmentID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
            });

            builder.Entity<ReviewsModel>().ToTable("Reviews");
            builder.Entity<ReviewsModel>().HasKey(t => t.ReviewID);
            builder.Entity<ReviewsModel>(entity =>
            {
                entity.HasOne(a => a.Users)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.UserID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.Pharmacies)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.PharmacyID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.MedicalEquipments)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.MedicalEquipmentID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
            });
            builder.Entity<UserLogin>().HasData(
                new UserLogin {
                    UserID = 1 ,
                    UserName="admin",
                    Password="admin"
                }
                ) ;

            builder.Entity<UserModel>().HasData(
                new UserModel {
                    UserId = 1, 
                    UserName = "admin", 
                    Password = "admin", 
                    FullName="admin",
                    Sex="male",
                    Phone="12345678",
                    DoB=new DateTime(2000,01,01),
                    Address="Ha Noi",
                    Description="None",
                    Email="abc@gmail.com",
                    IsAdmin=true,
                    IsDelete=false,
                    Status=true
                }
                );
            builder.Entity<PharmacyModel>().HasData(
                new PharmacyModel
                {
                    PharmacyID = 1,
                    PharmacyName="Paradon",
                    Quantity=1,
                    Price=1,
                    Description="None",
                    ExpireDate=new DateTime(2023,12,12),
                    ImportDate=new DateTime(2022,12,12),
                    ImagePath=null,
                    ManufacturedDate= new DateTime(2023, 02, 12),
                    Manufacturer="DMP",
                    IsDelete=false,
                    Status=true

                }
                );
            builder.Entity<MedicalEquipmentModel>().HasData(
                new MedicalEquipmentModel
                {
                    MedicalEquipmentID = 1,
                    MedicalEquipmentName = "Paradon",
                    Quantity = 1,
                    Price = 1,
                    Description = "None",
                    MedicalEquipmentType=01,
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    IsDelete = false,
                    Status = true

                }
                );
            builder.Entity<EducationModel>().HasData(
                new EducationModel
                {
                    EducationId =1,
                    EducationName="School",
                    ShortDescription="None",
                    Description="None",
                    CreatedDate=new DateTime(2023,01,01),
                    IsDelete=false,
                    Status=true,
                    UserID=1
                }
                );
            builder.Entity<OrdersModel>().HasData(
                new OrdersModel
                {
                    OrderID=1,
                    CreatedDate = new DateTime(2022,01,01),
                    MedicalEquipmentID=1,
                    PharmacyID=1,
                    UserID=1
                }
                );
            builder.Entity<ReviewsModel>().HasData(
                new ReviewsModel
                {
                    ReviewID = 1,
                    CreatedDate = new DateTime(2022, 01, 01),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                }
                );

        }

    }
}
