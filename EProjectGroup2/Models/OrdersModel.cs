﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class OrdersModel
    {
        [Key]
        public int OrderID { get; set; }
        public int? UserID { get; set; }
        public int? MedicalEquipmentID { get; set; }
        public int? PharmacyID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public UserModel? Users { get; set; }
        public PharmacyModel? Pharmacies { get; set; }
        public MedicalEquipmentModel? MedicalEquipments { get; set; }
    }
}
