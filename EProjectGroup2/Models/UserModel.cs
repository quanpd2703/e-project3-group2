﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace EProjectGroup2.Models
{
    public class UserModel
    { 
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? Description { get; set; }
        public string? FullName { get; set; }
        public DateTime? DoB { get; set; }
        public string? Sex { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public bool? IsAdmin { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public virtual ICollection<EducationModel>? Educations { get; set; }
        public virtual ICollection<OrdersModel>? Orders { get; set; }
        public virtual ICollection<ReviewsModel>? Reviews { get; set; }
    }
}
