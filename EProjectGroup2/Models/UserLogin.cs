﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class UserLogin
    {
        [Key]
        public int UserID { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
    }
}
