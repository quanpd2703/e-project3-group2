﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace EProjectGroup2.Models
{
    public class MedicalEquipmentModel
    {
        [Key]
        public int MedicalEquipmentID { get; set; }
        public string? MedicalEquipmentName { get; set; }
        public int? MedicalEquipmentType { get; set; }
        public string? Description { get; set; }
        public int? Quantity { get; set; }
        public float? Price { get; set; }
        public string? ImagePath { get; set; }
        public DateTime? ImportDate { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public string? Manufacturer { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public virtual ICollection<ReviewsModel>? Reviews { get; set; }
        public virtual ICollection<OrdersModel>? Orders { get; set; }
    }
}
