﻿using Microsoft.AspNetCore.Identity;

namespace EProjectGroup2.Models.DataTransferObject.UserModelDto
{
    public class AddUserModelDto : IdentityUser<int>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? Description { get; set; }
        public string? FullName { get; set; }
        public DateTime? DoB { get; set; }
        public string? Sex { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public bool? IsAdmin { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
